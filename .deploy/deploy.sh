#!/bin/bash

CONFIG_DIR=$(pwd)/.deploy

# Import env variables
. $CONFIG_DIR/.env

# Build the application and copy files to server through ssh
. $CONFIG_DIR/build_and_deploy.sh

# NgInx server
echo "---> Configuring NgInx <---"
echo $PASSWORD | scp $SCP_PORT_ALIAS -Cr $ROOT_DIR/.deploy/devops-js-app.conf $SSH_USER_ALIAS:/etc/nginx/sites-available/devops-js-app.conf

ssh $SSH_ALIAS "echo $PASSWORD | sudo -S rm /etc/nginx/sites-enabled/devops-js-app.conf"
ssh $SSH_ALIAS "echo $PASSWORD | sudo -S ln -s /etc/nginx/sites-available/devops-js-app.conf /etc/nginx/sites-enabled/devops-js-app.conf"

echo "---> Restarting NgInx service <---"
ssh $SSH_ALIAS "echo $PASSWORD | sudo -S systemctl restart nginx.service"
