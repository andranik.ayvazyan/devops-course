#!/bin/bash

check_remote_dir_exists() {
  echo "Check if remote directories exist"

  if ssh $SSH_ALIAS "[ ! -d $1 ]"; then
    echo "Creating: $1"
	  ssh $SSH_ALIAS "bash -c 'mkdir -p $1 && chown -R $SSH_USER: $1'"
  else
    echo "Clearing: $1"
    ssh $SSH_ALIAS "rm -rf $1/*"
  fi
}

check_remote_dir_exists $SERVER_REMOTE_DIR
check_remote_dir_exists $CLIENT_REMOTE_DIR

echo "---> Building and copying server files - START <---"
echo $SERVER_HOST_DIR
cd $SERVER_HOST_DIR && npm run build
scp $SCP_PORT_ALIAS -Cr dist/ package.json $SSH_USER_ALIAS:$SERVER_REMOTE_DIR
echo "---> Building and transfering server - COMPLETE <---"

echo "---> Building and transfering client files, cert and ngingx config - START <---"
echo $CLIENT_HOST_DIR
cd $CLIENT_HOST_DIR && npm run build && cd ../
scp $SCP_PORT_ALIAS -Cr $CLIENT_HOST_DIR/dist/* $ROOT_DIR/.cert/* $SSH_USER_ALIAS:$CLIENT_REMOTE_DIR
echo "---> Building and transfering - COMPLETE <---"
